package com.school.basetemplate.controllers;

import com.school.basetemplate.service.PetService;
import io.swagger.api.PetApi;
import io.swagger.model.ModelApiResponse;
import io.swagger.model.Pet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PetController implements PetApi {

    private final PetService petService;

    @Override
    public ResponseEntity<Void> addPet(Pet body) {
        petService.addPet(body);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deletePet(Long petId, String apiKey) {
        //check api key
        petService.deletePet(petId);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Pet>> findPetsByStatus(List<String> status) {
        return ResponseEntity.ok(petService.getPetsByStatus(status));
    }

    @Override
    public ResponseEntity<List<Pet>> findPetsByTags(List<String> tags) {
        return ResponseEntity.ok(petService.getPetsByTags(tags));
    }

    @Override
    public ResponseEntity<Pet> getPetById(Long petId) {
        return ResponseEntity.ok(petService.getPetById(petId));
    }

    @Override
    public ResponseEntity<Void> updatePet(Pet body) {
        petService.updatePet(body);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> updatePetWithForm(Long petId, String name, String status) {
        petService.updatePet(petId, name, status);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<ModelApiResponse> uploadFile(Long petId, String additionalMetadata, MultipartFile file) {
        return PetApi.super.uploadFile(petId, additionalMetadata, file);
    }
}
