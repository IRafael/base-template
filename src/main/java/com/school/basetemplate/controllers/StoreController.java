package com.school.basetemplate.controllers;

import io.swagger.api.StoreApi;
import io.swagger.model.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class StoreController implements StoreApi {

    @Override
    public ResponseEntity<Void> deleteOrder(String orderId) {
        return StoreApi.super.deleteOrder(orderId);
    }

    @Override
    public ResponseEntity<Map<String, Integer>> getInventory() {
        return StoreApi.super.getInventory();
    }

    @Override
    public ResponseEntity<Order> getOrderById(Long orderId) {
        return StoreApi.super.getOrderById(orderId);
    }

    @Override
    public ResponseEntity<Order> placeOrder(Order body) {
        return StoreApi.super.placeOrder(body);
    }
}
