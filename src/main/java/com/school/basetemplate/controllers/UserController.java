package com.school.basetemplate.controllers;

import io.swagger.api.UserApi;
import io.swagger.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController implements UserApi {

    @Override
    public ResponseEntity<Void> createUser(User body) {
        return UserApi.super.createUser(body);
    }

    @Override
    public ResponseEntity<Void> createUsersWithArrayInput(List<User> body) {
        return UserApi.super.createUsersWithArrayInput(body);
    }

    @Override
    public ResponseEntity<Void> createUsersWithListInput(List<User> body) {
        return UserApi.super.createUsersWithListInput(body);
    }

    @Override
    public ResponseEntity<Void> deleteUser(String username) {
        return UserApi.super.deleteUser(username);
    }

    @Override
    public ResponseEntity<User> getUserByName(String username) {
        return UserApi.super.getUserByName(username);
    }

    @Override
    public ResponseEntity<String> loginUser(String username, String password) {
        return UserApi.super.loginUser(username, password);
    }

    @Override
    public ResponseEntity<Void> logoutUser() {
        return UserApi.super.logoutUser();
    }

    @Override
    public ResponseEntity<Void> updateUser(String username, User body) {
        return UserApi.super.updateUser(username, body);
    }
}
