package com.school.basetemplate.controllers;

import com.school.basetemplate.model.Person;
import com.school.basetemplate.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @GetMapping(value = "/getPerson/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable(name = "id") String id) {
        final Person person = personService.getById(id);
        return ResponseEntity.ok(person);
    }

    @GetMapping(value = "/getPersons")
    public ResponseEntity<List<Person>> getPersons() {
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping(value = "/createPerson")
    public ResponseEntity<Void> createPerson(@RequestBody Person body) {
        personService.save(body);
        return ResponseEntity.ok().build();
    }

}
