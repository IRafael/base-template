package com.school.basetemplate;

import com.school.basetemplate.controllers.PersonController;
import com.school.basetemplate.model.POPerson;
import com.school.basetemplate.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class BaseTemplateApplication implements CommandLineRunner {

    private final PersonRepository personRepository;
    private final PersonController personController;

    public static void main(String[] args) {
        SpringApplication.run(BaseTemplateApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
