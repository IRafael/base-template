package com.school.basetemplate.service;

import com.school.basetemplate.model.Person;
import com.school.basetemplate.model.POPerson;
import com.school.basetemplate.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepository personRepository;

    public Person getById(final String id) {
        final Optional<POPerson> byId = personRepository.findById(UUID.fromString(id));
        if (byId.isPresent()) {
            final POPerson poPerson = byId.get();
            return getPerson(poPerson);
        }
        return null;
    }

    private static Person getPerson(POPerson poPerson) {
        return new Person(poPerson.getName(), poPerson.getSurname());
    }

    public List<Person> getAll() {
        return personRepository.findAll().stream().map(PersonService::getPerson).collect(Collectors.toList());
    }

    public void save(Person body) {
        final POPerson poPerson = new POPerson();
        poPerson.setName(body.getName());
        poPerson.setSurname(body.getSurname());
        personRepository.save(poPerson);
    }
}
