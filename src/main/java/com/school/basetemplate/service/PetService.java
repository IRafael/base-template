package com.school.basetemplate.service;

import com.school.basetemplate.model.POPet;
import com.school.basetemplate.model.POPhotoUrl;
import com.school.basetemplate.model.POTag;
import com.school.basetemplate.repository.PetRepository;
import com.school.basetemplate.repository.TagRepository;
import io.swagger.model.Category;
import io.swagger.model.Pet;
import io.swagger.model.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class PetService {

    private final PetRepository petRepository;
    private final TagRepository tagRepository;

    public void addPet(final Pet body) {
        final POPet pet = new POPet();
        pet.setName(body.getName());
        pet.setStatus(body.getStatus().toString());
        pet.setCategoryId(body.getCategory().getId());
        pet.setCategoryName(body.getCategory().getName());

        final Set<POPhotoUrl> photoUrls = body.getPhotoUrls().stream().map(url -> {
            final POPhotoUrl photoUrl = new POPhotoUrl();
            photoUrl.setUrl(url);
            photoUrl.setPet(pet);
            return photoUrl;
        }).collect(Collectors.toSet());
        pet.setPhotoUrls(photoUrls);

        final Set<POTag> tags = body.getTags().stream().map(tag -> {
            final Optional<POTag> tagById = tagRepository.findById(tag.getId());
            if (tagById.isEmpty()) {
                POTag createdTag = new POTag();
                createdTag.setName(tag.getName());
                return tagRepository.save(createdTag);
            } else {
                return tagById.get();
            }
        }).collect(Collectors.toSet());
        pet.setTags(tags);

        petRepository.save(pet);
    }

    public void deletePet(final Long petId) {
        petRepository.findById(petId).ifPresent(petRepository::delete);
    }

    public List<Pet> getPetsByStatus(final List<String> status) {
        return status
                .stream()
                .map(petRepository::findByStatus)
                .flatMap(Collection::stream)
                .map(poPet -> {
                    final Pet pet = new Pet();
                    pet.setId(poPet.getId());
                    pet.setStatus(Pet.StatusEnum.fromValue(poPet.getStatus()));
                    pet.setName(poPet.getName());
                    pet.setTags(poPet.getTags().stream().map(poTag -> {
                        final Tag tag = new Tag();
                        tag.setId(poTag.getId());
                        tag.setName(poTag.getName());
                        return tag;
                    }).collect(Collectors.toList()));

                    final Category category = new Category();
                    category.setId(poPet.getCategoryId());
                    category.setName(poPet.getCategoryName());
                    pet.setCategory(category);

                    pet.setPhotoUrls(poPet.getPhotoUrls().stream().map(POPhotoUrl::getUrl).collect(Collectors.toList()));
                    return pet;
                })
                .toList();
    }

    public List<Pet> getPetsByTags(final List<String> tags) {
        return null;
    }

    public Pet getPetById(final Long petId) {
        return null;
    }

    public void updatePet(final Pet body) {

    }

    public void updatePet(final Long petId, final String name, final String status) {

    }
}
