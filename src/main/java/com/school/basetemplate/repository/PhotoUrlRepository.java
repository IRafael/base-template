package com.school.basetemplate.repository;

import com.school.basetemplate.model.POPhotoUrl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoUrlRepository extends JpaRepository<POPhotoUrl, Long> {
}