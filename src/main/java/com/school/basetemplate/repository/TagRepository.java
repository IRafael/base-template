package com.school.basetemplate.repository;

import com.school.basetemplate.model.POTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<POTag, Long> {
}