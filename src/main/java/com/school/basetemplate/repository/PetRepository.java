package com.school.basetemplate.repository;

import com.school.basetemplate.model.POPet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetRepository extends JpaRepository<POPet, Long> {
    List<POPet> findByStatus(String status);
}