CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

CREATE TABLE pet
(
    id            BIGINT NOT NULL,
    category_id   BIGINT,
    category_name VARCHAR(255),
    name          VARCHAR(255),
    status        VARCHAR(255),
    CONSTRAINT pk_pet PRIMARY KEY (id)
);

CREATE TABLE pet_tags
(
    pet_id  BIGINT NOT NULL,
    tags_id BIGINT NOT NULL,
    CONSTRAINT pk_pet_tags PRIMARY KEY (pet_id, tags_id)
);

CREATE TABLE photo_url
(
    id     BIGINT NOT NULL,
    url    VARCHAR(255),
    pet_id BIGINT,
    CONSTRAINT pk_photo_url PRIMARY KEY (id)
);

CREATE TABLE tag
(
    id   BIGINT NOT NULL,
    name VARCHAR(255),
    CONSTRAINT pk_tag PRIMARY KEY (id)
);

ALTER TABLE photo_url
    ADD CONSTRAINT FK_PHOTO_URL_ON_PET FOREIGN KEY (pet_id) REFERENCES pet (id);

ALTER TABLE pet_tags
    ADD CONSTRAINT fk_pet_tags_on_p_o_pet FOREIGN KEY (pet_id) REFERENCES pet (id);

ALTER TABLE pet_tags
    ADD CONSTRAINT fk_pet_tags_on_p_o_tag FOREIGN KEY (tags_id) REFERENCES tag (id);