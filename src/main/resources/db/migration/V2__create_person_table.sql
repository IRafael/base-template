CREATE TABLE po_person
(
    id      UUID NOT NULL,
    name    VARCHAR(255),
    surname VARCHAR(255),
    CONSTRAINT pk_po_person PRIMARY KEY (id)
);

DROP TABLE test CASCADE;